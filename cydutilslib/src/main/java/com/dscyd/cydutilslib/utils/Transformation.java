package com.dscyd.cydutilslib.utils;

import android.text.format.Time;

import java.util.Calendar;

/**
 * Created by gxl on 2017/3/16.
 */

public class Transformation
{
    private static Time time;
    public static  void xingqi()
    {

    }
    //返回星期几
    public static String getWeek(){
        Calendar cal = Calendar.getInstance();
        int i = cal.get(Calendar.DAY_OF_WEEK);
        switch (i) {
            case 1:
                return "星期日";
            case 2:
                return "星期一";
            case 3:
                return "星期二";
            case 4:
                return "星期三";
            case 5:
                return "星期四";
            case 6:
                return "星期五";
            case 7:
                return "星期六";
            default:
                return "";
        }


    }
    public static String getMonth()
    {
        time = new Time();
        time.setToNow();
        int[] solarToLunar = LunarCalendar.solarToLunar(time.year, (time.month + 1), time.monthDay);
        int i = solarToLunar[3];
        String yuefen = new String();
        if (i==0)
        {
            switch (solarToLunar[1]-1) {

                case 0:
                    yuefen = "一月";
                    break;
                case 1:
                    yuefen = "二月";
                    break;
                case 2:
                    yuefen = "三月";
                    break;
                case 3:
                    yuefen = "四月";
                    break;
                case 4:
                    yuefen = "五月";
                    break;
                case 5:
                    yuefen = "六月";
                    break;
                case 6:
                    yuefen = "七月";
                    break;
                case 7:
                    yuefen = "八月";
                    break;
                case 8:
                    yuefen = "九月";
                    break;
                case 9:
                    yuefen = "十月";
                    break;
                case 10:
                    yuefen = "十一月";
                    break;
                case 11:
                    yuefen = "十二月";
                    break;
            }
        }else
        {
            switch (solarToLunar[1]-1) {

                case 0:
                    yuefen = "一月";
                    break;
                case 1:
                    yuefen = "二月";
                    break;
                case 2:
                    yuefen = "三月";
                    break;
                case 3:
                    yuefen = "四月";
                    break;
                case 4:
                    yuefen = "五月";
                    break;
                case 5:
                    yuefen = "六月";
                    break;
                case 6:
                    yuefen = "七月";
                    break;
                case 7:
                    yuefen = "八月";
                    break;
                case 8:
                    yuefen = "九月";
                    break;
                case 9:
                    yuefen = "十月";
                    break;
                case 10:
                    yuefen = "十一月";
                    break;
                case 11:
                    yuefen = "十二月";
                    break;
                case 12:
                    yuefen = "十三月";
            }

        }
        int tianshu = 0;
        if (i == 1)
        {
            tianshu = LunarCalendar.daysInMonth(time.year, time.month+1, true);

        }else
        {
            tianshu = LunarCalendar.daysInMonth(time.year, time.month+1, false);
        }

        if (tianshu == 30) {
            switch (solarToLunar[2]-1) {
                case 0:
                    return  ("农历" + yuefen + "初一");

                case 1:
                    return ("农历" + yuefen + "初二");
                case 2:
                    return ("农历" + yuefen + "初三");
                case 3:
                    return ("农历" + yuefen + "初四");
                case 4:
                    return ("农历" + yuefen + "初五");
                case 5:
                    return ("农历" + yuefen + "初六");
                case 6:
                    return ("农历" + yuefen + "初七");
                case 7:
                    return ("农历" + yuefen + "初八");
                case 8:
                    return ("农历" + yuefen + "初九");
                case 9:
                    return ("农历" + yuefen + "初十");
                case 10:
                    return ("农历" + yuefen + "十一");
                case 11:
                    return ("农历" + yuefen + "十二");
                case 12:
                    return ("农历" + yuefen + "十三");
                case 13:
                    return ("农历" + yuefen + "十四");
                case 14:
                    return ("农历" + yuefen + "十五");
                case 15:
                    return ("农历" + yuefen + "十六");
                case 16:
                    return ("农历" + yuefen + "十七");
                case 17:
                    return ("农历" + yuefen + "十八");
                case 18:
                    return ("农历" + yuefen + "十九");
                case 19:
                    return ("农历" + yuefen + "二十");
                case 20:
                    return ("农历" + yuefen + "廿一");
                case 21:
                    return ("农历" + yuefen + "廿二");
                case 22:
                    return ("农历" + yuefen + "廿三");
                case 23:
                    return ("农历" + yuefen + "廿四");
                case 24:
                    return ("农历" + yuefen + "廿五");
                case 25:
                    return ("农历" + yuefen + "廿六");
                case 26:
                    return ("农历" + yuefen + "廿七");
                case 27:
                    return ("农历" + yuefen + "廿八");
                case 28:
                    return ("农历" + yuefen + "廿九");
                case 29:
                    return ("农历" + yuefen + "三十");
            }
        } else if (tianshu == 29){
            switch (solarToLunar[2]-1) {
                case 0:
                    return ("农历" + yuefen + "初一");
                case 1:
                    return ("农历" + yuefen + "初二");
                case 2:
                    return ("农历" + yuefen + "初三");
                case 3:
                    return ("农历" + yuefen + "初四");
                case 4:
                    return ("农历" + yuefen + "初五");
                case 5:
                    return ("农历" + yuefen + "初六");
                case 6:
                    return ("农历" + yuefen + "初七");
                case 7:
                    return ("农历" + yuefen + "初八");
                case 8:
                    return ("农历" + yuefen + "初九");
                case 9:
                    return ("农历" + yuefen + "初十");
                case 10:
                    return ("农历" + yuefen + "十一");
                case 11:
                    return ("农历" + yuefen + "十二");
                case 12:
                    return ("农历" + yuefen + "十三");
                case 13:
                    return ("农历" + yuefen + "十四");
                case 14:
                    return ("农历" + yuefen + "十五");
                case 15:
                    return ("农历" + yuefen + "十六");
                case 16:
                    return ("农历" + yuefen + "十七");
                case 17:
                    return ("农历" + yuefen + "十八");
                case 18:
                    return ("农历" + yuefen + "十九");
                case 19:
                    return ("农历" + yuefen + "二十");
                case 20:
                    return ("农历" + yuefen + "廿一");
                case 21:
                    return ("农历" + yuefen + "廿二");
                case 22:
                    return ("农历" + yuefen + "廿三");
                case 23:
                    return ("农历" + yuefen + "廿四");
                case 24:
                    return ("农历" + yuefen + "廿五");
                case 25:
                    return ("农历" + yuefen + "廿六");
                case 26:
                    return ("农历" + yuefen + "廿七");
                case 27:
                    return ("农历" + yuefen + "廿八");
                case 28:
                    return ("农历" + yuefen + "廿九");

            }
        }
        return  null;
    }
    //农历月份
    static String[] nlMonth = {"正","二","三","四","五","六","七","八","九","十","十一","十二"};

    //农历日
    static String[] nlday = {"初一","初二","初三","初四","初五","初六","初七","初八","初九","初十",
            "十一","十二","十三","十四","十五","十六","十七","十八","十九","二十",
            "廿一","廿二","廿三","廿四","廿五","廿六","廿七","廿八","廿九","三十"};

}
